# Create ACI Network
module "aci-network" {
  source             = "./module/aci/demo-ist-aci"
  tenant_name        = "demo_ist_aci"
  tenant_description = "created by TE Cloud Agent"

  vmm_domain = "tokyo_hx_vmm"

  web_bd_subnet = "192.168.1.254/24"
  app_bd_subnet = "192.168.2.254/24"
}

# Create VMs (ACI End Points)
module "web-vm" {
  source     = "./module/vsphere/vm-medium"
  datacenter = "manta-tokyo"
  cluster    = "manta-tokyo-hx-cluster"
  datastore  = "ds1"
  portgroup  = "demo_ist_aci|demo_ap|web_epg"

  vm_template = "ubuntu-temp"

  host_name  = "web-vm"
  address_v4 = "192.168.1.1/24"
  gateway_v4 = "192.168.1.254"

#   depends_on = [
#     module.aci-network
#   ]
}

module "app-vm" {
  source     = "./module/vsphere/vm-medium"
  datacenter = "manta-tokyo"
  cluster    = "manta-tokyo-hx-cluster"
  datastore  = "ds1"
  portgroup  = "demo_ist_aci|demo_ap|app_epg"

  vm_template = "ubuntu-temp"

  host_name  = "app-vm"
  address_v4 = "192.168.2.1/24"
  gateway_v4 = "192.168.2.254"

#   depends_on = [
#     module.aci-network
#   ]
}
