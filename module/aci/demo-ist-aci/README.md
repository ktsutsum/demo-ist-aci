# Terraform-Module: Demo Network on Cisco ACI

## Requirements: providers
- (CiscoDevNet/aci)[https://registry.terraform.io/providers/CiscoDevNet/aci/latest]

## Requirements: TF Cloud secret or tfvars
- APIC URL
- APIC User
- APIC Password
\* Default: Not validate the certification.

## Variables
| Name | Type | Descriptions |
| ----------- | ----------- | ----------- |
| tenant_name | String | ACI Tenant Name |
| tenant_description | String | ACI Tenant Description (Default: demo_ist_aci) |
| vmm_domain | String | VMM Domain is assigned to EPGs |
| web_bd_subnet | String | One IPv4 Subnet for WEB-EPG (ex. "192.168.1.254/24") |
| app_bd_subnet | String | One IPv4 Subnet for APP-EPG (ex. "192.168.2.254/24") |