# For vSphere
variable "VSPHERE_USERNAME" {
  type = string
}

variable "VSPHERE_PASSWORD" {
  type = string
}

variable "VSPHERE_URL" {
  type = string
}

# For ACI Provider
variable "APIC_USERNAME" {
  type = string
}

variable "APIC_PASSWORD" {
  type = string
}

variable "APIC_URL" {
  type = string
}