terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

provider "vsphere" {
  user                 = var.VSPHERE_USERNAME
  password             = var.VSPHERE_PASSWORD
  vsphere_server       = var.VSPHERE_URL
  allow_unverified_ssl = true
}

provider "aci" {
  username = var.APIC_USERNAME
  password = var.APIC_PASSWORD
  url      = var.APIC_URL
  insecure = true
}
