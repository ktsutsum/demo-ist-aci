# demo-ist-aci
Demo: Cisco Instersight Service for Hashicorp Terraform (IST) with Cisco ACI.

## Getting started
### Terraform Cloud
1. Create your Terraform Cloud account [Terraform Cloud](https://app.terraform.io/).  
   You MUST register as a <font color="Red"> BUSINESS USER </font> to use a cloud agent.
### Cisco Intersight
1. Create your [Cisco Intersight](https://intersight.com) account.
2. Deploy the Intersight Assist in your vSphere.
3. Claim the Intersight Assist.
4. Claim the Terraform Cloud account.
5. Deploy a Terraform Cloud Agent from Intersight.
### Your Workspace
1. Folk this project and clone in your workspace.

## Usage
*TBD
